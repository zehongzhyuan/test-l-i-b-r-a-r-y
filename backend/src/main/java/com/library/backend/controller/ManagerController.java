package com.library.backend.controller;

import com.library.backend.entity.Manager;
import com.library.backend.model.Result;
import com.library.backend.repository.ManagerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.library.backend.utils.Constant.FAILE_CODE;
import static com.library.backend.utils.Constant.SUCCESS_CODE;

@RestController
@RequestMapping("/manager")
public class ManagerController {

    @Autowired
    private ManagerRepository managerRepository;

    @PostMapping("/login")
    public Result managerLogin(@RequestBody Manager manager) {
        Manager manager1 = managerRepository.findByUsername(manager.getUsername());
        if (manager1 != null && DigestUtils.md5DigestAsHex(manager.getPassword().getBytes()).equals(manager1.getPassword())) {
            return new Result(SUCCESS_CODE, "登录成功");
        } else {
            return new Result(FAILE_CODE, "登录失败");
        }

    }
}
